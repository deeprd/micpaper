%\documentclass[draft]{sig-alternate}
\documentclass{sig-alternate}

\special{papersize=8.5in,11in}
\setlength{\pdfpageheight}{\paperheight}
\setlength{\pdfpagewidth}{\paperwidth}

\usepackage{amssymb,amsmath}

\newtheorem{defi}{Definition}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{xcolor}

\usepackage{xspace}

\newcommand{\inred}[1]{{\color{red}#1}}
\newcommand{\todo}[1]{\inred{#1}}

\newcommand{\wlo}{w.\,l.\,o.\,g.\xspace}
\newcommand{\eg}{e.\,g.\xspace}
\newcommand{\ie}{i.\,e.\xspace}
\newcommand{\st}{s.\,t.\xspace}
\newcommand{\uar}{u.\,a.\,r.\xspace}

\newcommand{\gcais}{\text{\sc GC-AIS}\xspace}

\newcommand{\E}[1]{\mathord{\textup{E}}\mathord{\left(#1\right)}}
\newcommand{\Esmall}[1]{\mathord{\textup{E}}\mathord{(#1)}}
\newcommand{\Einv}{\textup{E}^{-1}}
\newcommand{\Var}[1]{\text{Var}\left(#1\right)}
\newcommand{\Prob}[1]{\mathord{\textup{Pr}}\mathord{\left(#1\right)}}
\newcommand{\ProbIndex}[2]{\mathord{\textup{Pr}^{\text{#2}}}\mathord{\left(#1\right)}}
\newcommand{\bigO}[1]{\mathord{O}\mathord{\left(#1\right)}}
\newcommand{\littleo}[1]{\mathord{o}\mathord{\left(#1\right)}}
\newcommand{\bigOmega}[1]{\mathord{\Omega}\mathord{\left(#1\right)}}
\newcommand{\littleomega}[1]{\mathord{\omega}\mathord{\left(#1\right)}}
\newcommand{\bigTheta}[1]{\mathord{\Theta}\mathord{\left(#1\right)}}
\newcommand{\norm}[1]{\left|#1\right|}
\newcommand{\floor}[1]{\left\lfloor#1\right\rfloor}
\newcommand{\ceil}[1]{\left\lceil#1\right\rceil}
\newcommand{\hamming}[2]{\mathord{\textup{H}}\mathord{\left(#1, #2\right)}}
\newcommand{\N}{\ensuremath{{\mathbb N}}}

\setlength\textfloatsep{1\baselineskip plus 3pt minus 2pt}

\begin{document}

\conferenceinfo{GECCO'15,} {July 11-15, 2015, Madrid, Spain.}
    \CopyrightYear{2015}
    \crdata{TBA}
    \clubpenalty=10000
    \widowpenalty = 10000

\title{On the Effects of Incorporating Memory in GC-AIS\\for the Set Cover Problem}
\subtitle{[Track: AIS-ACHEM]}

\numberofauthors{3}
\author{
\alignauthor
xxxxxxx
       \affaddr{xxxxxxxxx}\\
       \affaddr{xxxxxxxxx}\\
       \affaddr{xxxxxxxxx}\\
       \email{xxxxxxxxx}
\alignauthor
xxxxxxx
       \affaddr{xxxxxxxxx}\\
       \affaddr{xxxxxxxxx}\\
       \affaddr{xxxxxxxxx}\\
       \email{xxxxxxxxx}
\alignauthor
xxxxxxx
       \affaddr{xxxxxxxxx}\\
       \affaddr{xxxxxxxxx}\\
       \affaddr{xxxxxxxxx}\\
       \email{xxxxxxxxx}
}

\maketitle
\begin{abstract}
Learning is an important part of the immune system by which the immune system maintains a memory of the infections it has encountered to protect against future attacks. In this paper we incorporate the mechanism of maintaining a memory in the recently proposed GC-AIS algorithm. We systematically create novel instances of the set cover problem by editing instances taken from the OR-library. By using an optimal known solution for the original instance as the memory, we analyse the affect of using memory with varying degrees of modifications to the original instances.
\end{abstract}

\category{I.2.8}{Artificial Intelligence}{Problem Solving, Control Methods, and Search}
\terms{Algorithms, Design, Experimentation, Performance}
\keywords{Artificial immune systems, set cover, dynamic environments, memory}

\section{Introduction}

Many real-world problems have characteristics that change over time in the form of changing objective functions, the problem instance itself
or the constraints~\cite{yang2013evolutionary}. Evolutionary dynamic optimisation (EDO) deals with solving these time dependent problems called dynamic optimisation
problems (DOP) using evolutionary computation methods. Several techniques have been developed for EDO to tackle these problems like
diversity approaches~\cite{tinos2007self}, memory-~\cite{yang2010genetic} and prediction-based~\cite{simoes2009improving} approaches, multi-populations~\cite{goh2009competitive} and self-adaptive approaches~\cite{ursem2000multinational}. If the DOP under consideration involves multiple objectives
then they are referred to as dynamic multi-objective optimisation problems (DMOP). A detailed survey of EDO techniques
and DMOP can be found in~\cite{yang2013evolutionary}.

Artificial immune systems (AIS) are randomised search heuristics developed from taking inspiration from 
the immune system of vertebrates. The immune system stands out from other biological 
systems due to the presence of several desirable properties combined together. It is due to these properties
like memory, anomaly detection and robustness that AIS have been applied to very different applications like machine learning,
robotics and optimisation.  De Castro and Timmis~\cite{de2002artificial} provide a detailed survey of applications. 

Some work in the field of EDO has been done using AIS. The pattern recognition artificial immune system (PRAIS) was introduced by Hart and Ross~\cite{hart1999immune} for dynamic scheduling problems 
which was capable of producing schedules rapidly when the environment changed. Gaspar and Collard's simple artificial immune system~\cite{gasper1999gas} based on the idiotypic network theory was tested on a dynamic pattern matching problem was shown that their system maintained diversity over time and thus react to future changes. Sim{\~o}es and Costa~\cite{simoes2003immune} proposed a immune inspired genetic algorithm (ISGA) and tested its performance on dynamic version of the knapsack problem. They showed promising results when comparing their ISGA  with several EDO approaches of the time. De Franca et al.~\cite{de2005artificial} introduced an AIS called dopt-aiNet which was shown to find optima faster and maintain diversity along with dealing with dynamic environments. Yang~\cite{yang2006comparative} compared several immune inspired genetic algorithms along with their improved variant of ISGA on problems constructed by the DOP generator and showed that the original ISGA performed well with severe environment changes while their improved ISGA performed better when changes were not severe. Trojanowski and Wierzcho{\'n}~\cite{trojanowski2009immune} compared the performance of five immune-based algorithms on the moving peak DOPs.

During its lifetime an organism may encounter an infection multiple times. To overcome these infection in the 
future the immune system maintains a repertoire of cells called memory cells. When an infection occurs for the first time,
the immune cells which prove to be the most potent are selected to form memory cells and these are used to fight against 
subsequent attacks of the same infection. Since the total number of immune cells in the body is regulated, the immune cell 
repertoire reflects the infections faced by the organism over its lifetime~\cite{de2002artificial}. 

GC-AIS is a novel AIS introduced by Joshi et al.~\cite{Joshi2014}, which is inspired by recent research on the germinal centre reaction~\cite{zhang2013germinal}. 
Taking inspiration from the learning aspects of the immune system we incorporate memory in the GC-AIS algorithm. This new variant of GC-AIS
is tested on the set cover problem with a simple dynamic component. The dynamic component is created by altering the instances of the 
set cover problem taken from the OR-library~\cite{beasley1990or}, by adding, removing or editing subsets from each instance. \todo{It is shown that adding subsets
causes  while editing and removing subsets causes .}

\todo{CHECK: The outline of the paper is as follows: In Section 2 the set cover problem is introduced along with a description of GC-AIS. Section 3
details the experimental setup which includes details on the novel instance generation. In section 4 obtained results are presented 
along with a discussion and the paper is concluded in section 5 with remarks and conclusions.}

\section{Preliminaries }

\todo{Add some introduction. Give motivation why set cover is considered.}

\subsection{Set Cover Problem}

The set cover problem (SCP) can be defined as: Given a \textit{universe set} $U$, which consists of $m$ items, and another set $S$, which contains
$n$ subsets of $U$ and whose union equals $U$, the problem is to find the smallest subset of $S$, which covers $U$. A more formal definition is
as follows:

\begin{defi}
Let the set of $ m $ items $U:=\{u_{1},...,u_{m}\}$
denote the universe and let $S:=\{s_{1},...,s_{n}\}$ such that $ s_{i} \subseteq U $ for $ 1 \leq i \leq n  $ and  $\bigcup_{i=1}^{n}s_{i}=U$.  The unicost set cover problem can be defined as 
finding a selection $I \subseteq \{1,2,...,n\}$ such that $ \bigcup_{k \in I}{s_{k}} = U $ with minimum $\vert{I}\vert$ .
\end{defi}

SCP is a constrained single objective problem where the objective is to find the smallest subset of $S$ that covers $U$ with the 
constraint that the subset covers $U$.
It is a NP hard combinatorial optimisation problem, which has many practical applications, an important one
being scheduling~\cite{caprara2000algorithms}. Caprara et al.~\cite{caprara2000algorithms} provide a survey of techniques employed 
to solve the set cover problem. 

\todo{Can you say something why this is useful?
A multi-objective formulation can be obtained} by transforming the constraint as a
secondary objective~\cite{friedrich2010approximating}. Let vector $X=x_{1}x_{2} \cdots x_{n}$, with $x_{i} \in \{0,1\}$ for $1 \leq i \leq n$, denote a solution to the problem where $x_{i}=1$ if
set $s_{i}$ is in the solution and 0 otherwise. Let $N$ be the number of subsets selected in $X$ and $C$ be the number of 
elements left uncovered in $U$. The fitness function for this multi-objective formulation of SCP can now be defined as vector $F=\langle C,N \rangle$.
Joshi et al. \cite{Joshi2014} have investigated the performance of GC-AIS on the SCP using its multi-objective formulation and compared it with the parallel global simple evolutionary multi-objective optimiser. 

\todo{This comes somehow out of nowhere. Why are you introducing this here. Where do you have this definition from (reference)? I also think the definition is not entirely correct... A different way to formulate SCP is: 

\begin{defi}

Let a matrix $A_{ij}$ comprising $0$s and $1$s, having $m$ rows and $n$ columns where a column $j$ is said to cover a row $i$ if $a_{ij} = 1$. 
Let $x_{j} = 1$ if column $j$ is in the solution and $0$ otherwise. Minimise $\sum_{j=0}^{n}x_{j}$ subject to $\sum_{j=0}^{n}x_{j} \geq 1, \text{and } x_{j} \in \{0,1\} 
\text{ } j=1,\cdots,n$. 

\end{defi}}

\subsubsection{A Simple Dynamic Extension for SCP}

In order to convert the static SCP to a dynamic problem some form of time dependent change must be introduced.
\todo{Incomplete sentence? Based on models introduced in~\cite{chrissis1982dynamic}}

\todo{Unclear (probably related to the above definition):
\begin{defi}
The dynamic unicost SCP can be  stated as minimise $ \sum_{t=0}^{T} \sum_{j=0}^{n_{t}}x_{jt} \text{ subject to } \sum_{j=0}^{n}x_{jt} \geq 1 \text{ } t=1,\cdots,T$. 
\end{defi}}

Three different types of changes are introduced in this paper namely adding, removing or editing columns in the $m \times n$ matrix. 
According to Definition 1 these changes can be seen as adding elements to $S$, removing elements from $S$ or editing elements inside $S$. 
\todo{There seem to be a couple of details missing here...}
Some work using dynamic SCP has been done by Chrissis et al.~\cite{chrissis1982dynamic} for dynamic facility locations and Kodani et al.~\cite{singh2009dynamic} for real time fault diagnosis.  

\subsection{EDO and Memory-Based Approaches}

DOPs are characterised by time-dependent changes to the problem, which are usually in the form of a sequence of static problems linked 
by some dynamic rules or having a time dependent parameter in its expression. \todo{Reference?}
\todo{Repetition from the introduction - maybe move second sentence to introduction and remove the repetition here. Evolutionary dynamic optimisation is the research area focussed on solving dynamic optimisation problems using evolutionary computation.
EDO is a rapidly growing area of research and a lot of work has been done in this field in recent years~\cite{yang2013evolutionary}.}The key difference between DOPs and static problems is requirement of \todo{ECs?} to track changing optima for DOPs, \todo{New: rather than simply locating them.}

\todo{Why are you explaining these in so much detail here? Concentrate on memory approaches as one example. You are only doing this here. }To solve the issues of tracking optima several techniques have been developed in the EDO literature, the key ones being:
\begin{itemize}
\item Diversity schemes: Introduce diversity in an evolutionary algorithm (EA) after detecting a change or throughout the search of the algorithm, see, e\,g.~\cite{tinos2007self}.
\item Memory schemes: Stores good solutions and re-uses them at a later stage. Useful when changes are recurrent or periodical and old optima may be revisited. E.g.,~\cite{yang2010genetic}
\item Multi-population schemes: Maintains sub-populations concurrently which may handle separate areas of the search space, or separate tasks and communicate with each other. E.g.,~\cite{goh2009competitive}
\item Prediction schemes: Tries to learn patterns from previous experience and predict changes in the future. E.g.m~\cite{simoes2009improving}
\item Adaptive schemes: Uses self-adaptive behaviour of EAs to cope with changes. E.g.,~\cite{ursem2000multinational}

\end{itemize}


\subsubsection{Memory-Based Techniques for EDO}
These approaches are often used when the changes in the DOPs are recurrent or periodic in nature, therefore an old optima may be revisited in the future. \todo{References}
In such cases it makes sense to save old solutions as a form of memory and use them when an old optima is encountered again in order to save computation time.
Two main variants of memory approaches exist in literature namely: implicit and explicit memory. \todo{Reference} \todo{Where did you get the following from - I don't know what a multiploid or haploid is. Implicit memory is maintained by encoding the chromosome as 
multiploid instead of the more common haploid in EAs for static problems.} Explicit memory on the other hand involves an external storage of information. This information
may be a previously known good solutions \todo{This does not seem to be a proper sentence: where it is called direct memory or associative information such as environment conditions, state transition probabilities, probabilities of 
feasible regions, to name a few, where it is called indirect memory.} The memory is periodically updated by replacement and based on the current best information. \todo{Again repetition: Memory-based
approaches are effective for problems with periodic changes} and they help maintain diversity \todo{Why? Reference}. Some disadvantages of memory-based approaches are that they are only useful 
in cases when previous optima are revisited (\todo{not necessarily as you see in your results where you do not necessarily have the same optima}). The \todo{redundant coding???} of memory is not useful in cases when number of fluctuations is high. A review of memory-based approaches 
can be found in~\cite{yang2013evolutionary} \todo{Give the concrete chapter here.}.

\todo{Repetition: When the DOP in consideration has several competing objectives it is called a dynamic multi-objective optimisation problem (DMOP).} Branke~\cite{branke1999memory} used a explicit memory approach to store individuals in a finite memory, which uses a replacement approach when the memory becomes full. Yang~\cite{yang2006associative} proposed an associative scheme in where individuals along with a distribution scheme are stored in the memory. Zhang~\cite{zhang2011artificial} introduced the dynamic constrained multi-objective artificial immune system (DCMOAIS),
which consists of three modules: a problem detection module based on T-cells, a solution module based on B-cells and a storage module based on memory (M-cells). \todo{So these are all memory approaches in DMOP?}

\section{ Extended GC-AIS with Memory}
The germinal centre artificial immune system (GC-AIS)~\cite{Joshi2014} is a new AIS based on recent understanding of the germinal centre reaction in the immune
system. A germinal centre (GC) is a region in the immune system where a type of immune cells called B cells are presented with the invading pathogen in order to 
generate antibodies (Abs), which fight the infection.

When the body is attacked by a pathogen the number of \textit{GCs} begin to rise in order to generate \textit{Abs} which are capable of eradicating the 
pathogen. By continuous proliferation mutation and selection of B cells in the germinal centres their ability to bind with the pathogen increases and this reaction is able
to produce \textit{Abs}, which can successfully fight the infection. There is periodic communication between \textit{GCs} by transmitting \textit{Ab}s. Towards the 
stage when \textit{Abs} produced are capable to fight off the infection the number of \textit{GCs} starts declining. The GC-AIS is based on a new theory of selection 
in the GC reaction proposed by Zhang et al.~\cite{zhang2013germinal} according to which there is a competition between the mutating B cells and the \textit{Abs} and 
cells, which are unable to compete, die by the process of natural cell death (apoptosis). This can even lead to whole \textit{GCs} to disappear if cells within them cannot 
compete with \textit{Abs} from neighbours.

The GC-AIS is extended by a simple explicit memory component for the dynamic SCP proposed above. \todo{Why do you restrict this to 1? This memory is an finite store of size 1 which stores information about 
the best solution of the original SCP instance before changes have been applied to it.} The memory contains all the subsets from $S$ which have corresponding $1$s in the best known solution. This memory is then used to initialise the GC at the beginning of Algorithm \ref{algo:gcais}.

\begin{algorithm}
\caption{The \textsc{GC-AIS with memory}}\label{algo:gcais}
\begin{algorithmic}
\STATE \textbf{Let} $G^t$ denote the population of GCs at generation $t$ and $g^{t}_{i}$ the $i$-th GC in $G^t$.
\STATE Create GC pool $G^0= \{g_{1}^{0}\}$ and initialise $g_{1}^{0}$ from memory. Let $t: = 0$.
\LOOP
\FOR{each GC $g^{t}_{i}$ in pool $G^t$ in parallel}
\STATE Create offspring $y_i$ of individual $g^{t}_{i}$ by standard bit mutation.
\ENDFOR
\STATE Add all $y_i$ to $G^{t}$, remove all dominated solutions from $G^{t}$ and let $G^{t+1}=G^{t}$.
\STATE  \textbf{Let} $t=t+1$.
\ENDLOOP
\STATE Save best solution information as memory
\end{algorithmic}
\end{algorithm} 

Based on Algorithm \ref{algo:gcais} the steps in the GC-AIS can be described as follows: A single GC is created at the start, which contains one individual 
that represents a B-cell. This \textit{GC} is initialised by an external memory component. By standard bit mutation of B-cells in the GC offspring are created,
 where standard bit mutation refers to each bit being flipped with probability $1/n$. There is a migration of fitness values of offspring
 between \textit{GC}s at every generation which corresponds to migration of \textit{Ab}s. After this communication all dominated solutions are 
 deleted, which can be seen as cell death of B cells which cannot compete  with neighbours and the surviving offspring form new \textit{GC}s. Thus the model 
 is dynamic in nature as number of \textit{GC}s can change with time. At every generation of the GC-AIS maintains a set of non-dominating solutions.
 

\section{Experimental setup}

\subsection{Novel Instance generation}

\section{ Results and Discussion}

\subsection{Adding rows}
\subsection{Removing rows}
\subsection{Editing rows}

\section{Conclusions}

\bibliographystyle{abbrv}
\bibliography{sigproc} 

\end{document}
