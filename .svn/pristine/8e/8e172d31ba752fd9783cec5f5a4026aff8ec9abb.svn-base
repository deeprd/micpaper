% This is "sig-alternate.tex" V2.0 May 2012
% This file should be compiled with V2.5 of "sig-alternate.cls" May 2012
%
% This example file demonstrates the use of the 'sig-alternate.cls'
% V2.5 LaTeX2e document class file. It is for those submitting
% articles to ACM Conference Proceedings WHO DO NOT WISH TO
% STRICTLY ADHERE TO THE SIGS (PUBS-BOARD-ENDORSED) STYLE.
% The 'sig-alternate.cls' file will produce a similar-looking,
% albeit, 'tighter' paper resulting in, invariably, fewer pages.
%
% ----------------------------------------------------------------------------------------------------------------
% This .tex file (and associated .cls V2.5) produces:
%       1) The Permission Statement
%       2) The Conference (location) Info information
%       3) The Copyright Line with ACM data
%       4) NO page numbers
%
% as against the acm_proc_article-sp.cls file which
% DOES NOT produce 1) thru' 3) above.
%
% Using 'sig-alternate.cls' you have control, however, from within
% the source .tex file, over both the CopyrightYear
% (defaulted to 200X) and the ACM Copyright Data
% (defaulted to X-XXXXX-XX-X/XX/XX).
% e.g.
% \CopyrightYear{2007} will cause 2007 to appear in the copyright line.
% \crdata{0-12345-67-8/90/12} will cause 0-12345-67-8/90/12 to appear in the copyright line.
%
% ---------------------------------------------------------------------------------------------------------------
% This .tex source is an example which *does* use
% the .bib file (from which the .bbl file % is produced).
% REMEMBER HOWEVER: After having produced the .bbl file,
% and prior to final submission, you *NEED* to 'insert'
% your .bbl file into your source .tex file so as to provide
% ONE 'self-contained' source file.
%
% ================= IF YOU HAVE QUESTIONS =======================
% Questions regarding the SIGS styles, SIGS policies and
% procedures, Conferences etc. should be sent to
% Adrienne Griscti (griscti@acm.org)
%
% Technical questions _only_ to
% Gerald Murray (murray@hq.acm.org)
% ===============================================================
%
% For tracking purposes - this is V2.0 - May 2012

\documentclass{sig-alternate}
\usepackage{amsmath}
\newtheorem{defi}{Definition}


\begin{document}
%
% --- Author Metadata here ---
\conferenceinfo{WOODSTOCK}{'97 El Paso, Texas USA}
%\CopyrightYear{2007} % Allows default copyright year (20XX) to be over-ridden - IF NEED BE.
%\crdata{0-12345-67-8/90/01}  % Allows default copyright data (0-89791-88-6/97/05) to be over-ridden - IF NEED BE.
% --- End of Author Metadata ---

\title{Alternate {\ttlit ACM} SIG Proceedings Paper in LaTeX
Format
%\titlenote{(Produces the permission block, and
%copyright information). For use with
%SIG-ALTERNATE.CLS. Supported by ACM.}
}
%%\subtitle{[Extended Abstract]
%\titlenote{A full version of this paper is available as
%\textit{Author's Guide to Preparing ACM SIG Proceedings Using
%\LaTeX$2_\epsilon$\ and BibTeX} at
%\texttt{www.acm.org/eaddress.htm}}}
%
% You need the command \numberofauthors to handle the 'placement
% and alignment' of the authors beneath the title.
%
% For aesthetic reasons, we recommend 'three authors at a time'
% i.e. three 'name/affiliation blocks' be placed beneath the title.
%
% NOTE: You are NOT restricted in how many 'rows' of
% "name/affiliations" may appear. We just ask that you restrict
% the number of 'columns' to three.
%
% Because of the available 'opening page real-estate'
% we ask you to refrain from putting more than six authors
% (two rows with three columns) beneath the article title.
% More than six makes the first-page appear very cluttered indeed.
%
% Use the \alignauthor commands to handle the names
% and affiliations for an 'aesthetic maximum' of six authors.
% Add names, affiliations, addresses for
% the seventh etc. author(s) as the argument for the
% \additionalauthors command.
% These 'additional authors' will be output/set for you
% without further effort on your part as the last section in
% the body of your article BEFORE References or any Appendices.

\numberofauthors{4} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Ben Trovato
%		\titlenote{Dr.~Trovato insisted his name be first.}\\
       \affaddr{Institute for Clarity in Documentation}\\
       \affaddr{1932 Wallamaloo Lane}\\
       \affaddr{Wallamaloo, New Zealand}\\
       \email{trovato@corporation.com}
% 2nd. author
\alignauthor
G.K.M. Tobin
%\titlenote{The secretary disavows any knowledge of this author's actions.}\\
       \affaddr{Institute for Clarity in Documentation}\\
       \affaddr{P.O. Box 1212}\\
       \affaddr{Dublin, Ohio 43017-6221}\\
       \email{webmaster@marysville-ohio.com}
% 3rd. author
\alignauthor Lars Th{\o}rv{\"a}ld
%\titlenote{This author is the one who did all the really hard work.}\\
       \affaddr{The Th{\o}rv{\"a}ld Group}\\
       \affaddr{1 Th{\o}rv{\"a}ld Circle}\\
       \affaddr{Hekla, Iceland}\\
       \email{larst@affiliation.org}
%\and  % use '\and' if you need 'another row' of author names
% 4th. author
}

% There's nothing stopping you putting the seventh, eighth, etc.
% author on the opening page (as the 'third row') but we ask,
% for aesthetic reasons that you place these 'additional authors'
%% in the \additional authors block, viz.
%\additionalauthors{Additional authors: John Smith (The Th{\o}rv{\"a}ld Group,
%email: {\texttt{jsmith@affiliation.org}}) and Julius P.~Kumquat
%(The Kumquat Consortium, email: {\texttt{jpkumquat@consortium.net}}).}
\date{30 July 1999}
% Just remember to make sure that the TOTAL number of authors
% is the number that will appear on the first page PLUS the
% number that will appear in the \additionalauthors section.

\maketitle
\begin{abstract}
This paper provides a sample of a \LaTeX\ document which conforms,
somewhat loosely, to the formatting guidelines for
ACM SIG Proceedings. It is an {\em alternate} style which produces
a {\em tighter-looking} paper and was designed in response to
concerns expressed, by authors, over page-budgets.
It complements the document \textit{Author's (Alternate) Guide to
Preparing ACM SIG Proceedings Using \LaTeX$2_\epsilon$\ and Bib\TeX}.
This source file has been written with the intention of being
compiled under \LaTeX$2_\epsilon$\ and BibTeX.

\end{abstract}

% A category with the (minimum) three required fields
\category{H.4}{Information Systems Applications}{Miscellaneous}diversity
%A category including the fourth, optional field follows...
\category{D.2.8}{Software Engineering}{Metrics}[complexity measures, performance measures]

\terms{Theory}

\keywords{ACM proceedings, \LaTeX, text tagging}

\section{Introduction}

Artificial immune Systems are randomized search heuristics developed from taking inspiration from 
the immune system of vertebrates. The immune system stands out from other biological 
systems due to the presence of several desirable properties combined together. It is due to these properties
like memory, anomaly detection and robustness that AIS have been applied to different applications like machine learning,
robotics, optimization Castro and Timmis~\cite{de2002artificial} provide a detailed survey of applications. 

Many real world problems have characteristics that change over time in the form of changing objective function, the problem instance itself
or the constraints~\cite{branke2001evolutionary}. Evolutionary dynamic optimization (EDO) deals with solving these time dependent problems called dynamic optimization
problems (DOP) using evolutionary computational methods. Several techniques have been developed for EDO to tackle these problems like
change detection, memory based approaches, prediction, multi-population and self-adaptive approaches. If the DOP under consideration involves multiple objectives
then they are referred to as dynamic multi-objective optimization problems. A detailed survey of EDO techniques and DMOP can be found in~\cite{yang2007evolutionary}.

During its lifetime an organism may encounter an infection multiple times. To overcome these infection in the 
future the immune system maintains a repertoire of cells called memory cells. When an infection occurs for the first time,
the immune cells which prove to be the most potent are selected to form memory cells and these are used to fight against 
subsequent attacks of the same infection. Since the total number of immune cells in the body is regulated, the immune cell 
repertoire reflects of the infections faced by the organism~\cite{de2002artificial}. 

GC-AIS is a novel AIS introduced by Joshi et al.~\cite{Joshi2014}, which is inspired by recent research on the Germinal center reaction~\cite{zhang2013germinal}. 
Taking inspiration from the learning aspects of the immune system we incorporate memory in the GC-AIS algorithm. This new variant of GC-AIS
is tested on the set-cover problem with a simple dynamic component. The dynamic component is created by altering the instances of the 
set-cover problem taken from the OR-library, by adding, removing or editing subsets from each instance. It is shown that adding subsets
causes  while editing and removing subsets causes .

The outline of the paper is as follows: In Section 2 the set-cover problem is introduced along with a description of GC-AIS. Section 3
details the experimental setup which includes details on the novel instance generation. In section 4 obtained results are presented 
along with a discussion and the paper is concluded in section 5 with remarks and conclusions.

\section{Preliminaries }

\subsection{Set cover problem}

The set cover problem (SCP) can be defined as: Given a \textit{universe set} $U$ which consists of $m$ items and another set $S$ which contains
$n$ subsets of $U$ and whose union equals $U$, the problem is to find the smallest subset of $S$ which covers $U$. A more formal definition is
as follows:

\begin{defi}
Let the set of $ m $ items $U:=\{u_{1},...,u_{m}\}$
denote the universe and let $S:=\{s_{1},...,s_{n}\}$ such that $ s_{i} \subseteq U $ for $ 1 \leq i \leq n  $ and  $\bigcup_{i=1}^{n}s_{i}=U$.  The unicost set cover problem can be defined as 
finding a selection $I \subseteq \{1,2,...,n\}$ such that $ \bigcup_{k \in I}{s_{k}} = U $ with minimum $\vert{I}\vert$ .
\end{defi}

 
This description is a constrained single objective problem where the objective is to find the smallest subset of $S$ that covers $U$ with the 
constraint that the subset covers $U$. A multi-objective formulation can be formulated by transforming the constraint as a
secondary objective~\cite{friedrich2010approximating}. Let vector $X=x_{1}x_{2} \cdots x_{n} \in {0,1}^n$ denote a solution to the problem where $x_{i}=1$ if
set $s_{i}$ is in the solution and 0 otherwise. Let $N$ be the number of subsets selected in $X$ and $C$ be the number of 
elements left uncovered in $U$. The fitness function for this multi-objective formulation of SCP can now be defined as vector $F=\langle C,N \rangle$.
SCP is a NP hard combinatorial optimization problem which has many practical applications, an important one
being scheduling~\cite{caprara2000algorithms}. Caprara et al.~\cite{caprara2000algorithms} provide a survey of techniques employed 
to solve the set cover problem.


\subsection{GC-AIS algorithm}


\subsection{Extending GC-AIS with memory}

\section{Experimental setup}

\subsection{Novel Instance generation}

\section{ Results and Discussion}

\subsection{Adding rows}
\subsection{Removing rows}
\subsection{Editing rows}

\section{Conclusions}
% The following two commands are all you need in the
% initial runs of your .tex file to
% produce the bibliography for the citations in your paper.
\bibliographystyle{abbrv}
\bibliography{sigproc}  % sigproc.bib is the name of the Bibliography in this case
% You must have a proper ".bib" file
%  and remember to run:
% latex bibtex latex latex
% to resolve all references
%
% ACM needs 'a single self-contained file'!
%
%APPENDICES are optional
%\balancecolumns
\appendix
%Appendix A
\section{Headings in Appendices}
\textit{within} an Appendix, start with \textbf{subsection} as the
highest level. Here is an outline of the body of this
document in Appendix-appropriate form:

\subsection{References}
Generated by bibtex from your ~.bib file.  Run latex,
then bibtex, then latex twice (to resolve references)
to create the ~.bbl file.  Insert that ~.bbl file into
the .tex source file and comment out
the command \texttt{{\char'134}thebibliography}.
% This next section command marks the start of
% Appendix B, and does not continue the present hierarchy
\section{More Help for the Hardy}
The sig-alternate.cls file itself is chock-full of succinct
and helpful comments.  If you consider yourself a moderately
experienced to expert user of \LaTeX, you may find reading
it useful but please remember not to change it.
%\balancecolumns % GM June 2007
% That's all folks!
\end{document}
