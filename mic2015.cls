\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mic2015}[2014/06/02 Class for MIC 2015 articles]
\LoadClass[a4paper,11pt,twoside]{article}
\RequirePackage{graphicx}
\RequirePackage[a4paper,hmargin=2.5cm,vmargin=2.5cm]{geometry}
\RequirePackage{times}
\RequirePackage{fancyhdr}
\RequirePackage[english]{babel}

\newcounter{@inst}
\newcounter{@auth}
\newcounter{auco}
\setcounter{@auth}{1}
\def\thanks#1{}
\def\institute#1{\gdef\@institute{#1}}
\def\inst#1{\unskip$^{#1}$}
\def\fnmsep{\unskip$^,$}
\def\email#1{{#1}}
\def\andname{and}
\def\lastandname{\unskip, and}
\def\lastand{\ifnum\value{auco}=2\relax
                 \unskip{} \andname\
              \else
                 \unskip \lastandname\
              \fi}%
 \def\and{\stepcounter{@auth}\relax
          \ifnum\value{@auth}=\value{auco}%
             \lastand 
          \else
             \unskip,
          \fi}
\def\date#1{}
\def\id#1{\gdef\@id{#1}}

\def\institutename{\par
 \begingroup
 \parskip=\z@
 \parindent=\z@
 \setcounter{@inst}{1}%
 \def\and{\par\stepcounter{@inst} \vskip 0.2cm%
 \noindent$^{\the@inst}$\enspace\ignorespaces}%
 \setbox0=\vbox{\def\thanks##1{}\@institute}%
 \ifnum\c@@inst=1\relax
 \else
   \setcounter{footnote}{\c@@inst}%
   \setcounter{@inst}{1}%
   \noindent$^{\the@inst}$\enspace
 \fi
 \ignorespaces
 \@institute\par
 \endgroup}

\renewcommand{\maketitle}{%
  \newpage
  \thispagestyle{fancy}
  \stepcounter{section}%
  \setcounter{section}{0}%
  \setcounter{subsection}{0}%
  \setcounter{figure}{0}
  \setcounter{table}{0}
  \setcounter{equation}{0}
  \setcounter{footnote}{0}
  \begin{center}
  {\fontsize{18}{24}\fontseries{b}\selectfont \@title}%
  \vskip 0.5cm
  \global\value{@inst}=\value{@auth}
  \global\value{auco}=\value{@auth}
  \setcounter{@auth}{1}
  {\noindent\ignorespaces\@author}
  \vskip 0.5cm
  {\small\institutename}  
  \end{center}
  \setcounter{footnote}{0}
}

\pagestyle{fancy}

\AtBeginDocument{
\lhead[\fancyplain{}{\@id--\thepage}]{\fancyplain{}{MIC 2015: The XI Metaheuristics International Conference}}
\rhead[\fancyplain{}{MIC 2015: The XI Metaheuristics International Conference}]{\fancyplain{}{\@id--\thepage}}
\lfoot[\fancyplain{}{Agadir, June 7-10, 2015}]{}
\cfoot{}
\rfoot[]{\fancyplain{}{Agadir, June 7-10, 2015}}
%\footrulewidth 0pt
}

